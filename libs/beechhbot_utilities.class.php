<?php
	
/**
*
*/
class beechbot_utilities {
	
	
	/**
	*
	*/
	public static function makeExcerpt($text, $length = 140) {
			
		return strlen($text) > $length -3 ? substr($text, 0, $length-3) . '...' : substr($text, 0, $length);
		
	} // makeExcerpt()
	
	
	/**
	*
	*/
	function cleanupEmptyP($content) {
		$pattern = "/<p>\&nbsp\;<\\/p>/";
		$content = preg_replace($pattern, '', $content);
		return $content;
	} // cleanupEmptyP()
	
	
	/**
	*
	*/
	public static function addDashHelper($args) {
		
		// $catID = null, $posts = null, $singularize = true, $name = null, $creationLinkOverride=null
		
		$ID = isset($args['ID']) ? $args['ID'] : -1;
		$type = isset($args['type']) ? $args['type'] : 'page'; // category, post, page
		
		$posts = isset($args['posts']) ? $args['posts'] : null;
		$name = isset($args['name']) ? $args['name'] : null; 
		
		if(is_null($name) && $type == 'category' && isset($args['ID']))
			$name = get_category($args['ID'])->name;
		
		$singularize = isset($args['singularize']) ? $args['singularize'] : true;
		$singular = $singularize && strtolower(substr($name, -1)) == 's' ? substr($name, 0, (strlen($name) - 1)) : $name;
		
		
		$creationLinkOverride = isset($args['creationLinkOverride']) ? $args['creationLinkOverride'] : null;
		
		$linkStyle = 'background:#e0e0e0;margin:0.25em 0;padding:0.25em 0 0;text-align:center;display:block;text-transform:uppercase;';
		
		echo '<div style="font-size:1.25em;line-height:1.6em;">';
			
		
		switch($type) {
			
			case('category'):
				$creationLink = !is_null($creationLinkOverride) ? $creationLinkOverride : 'post-new.php?prePop&prePop-cat=' . $ID;
				break;
				
			case('post'):
				$creationLink = !is_null($creationLinkOverride) ? $creationLinkOverride : 'post-new.php';
				break;
				
			case('page'):
			default:
				$creationLink = !is_null($creationLinkOverride) ? $creationLinkOverride : 'post-new.php?post_type=page';
				break;
			
		}
		
		echo '<a href="' . $creationLink . '" style="' . $linkStyle . '">+ ADD NEW ' . $singular . '</a>';

		if($posts) {
			
			foreach($posts as $k => $p) {
				
				echo '<div style="background:#eee;margin:0.25em 0;padding:0.25em 0.5em;">
						<span style="display:inline-block;">' . $p->post_title . '</span> 
						<span style="display:block;float:right;">
							<a href="' . get_edit_post_link($p->ID, '&') . '" style="position:relative;display:inline-block;padding:0 0.5em;">[edit]</a> 
							<a href="' . get_permalink($p->ID) . '" target="_blank" style="display:inline-block;">[view]</a>
						</span>
					<div style="clear:both;"></div></div>';
				
			}
			
			if(isset($args['showAll']))
				echo '<div style="padding:5px;margin-top:5px;border-top:1px dashed #eee;"><a href="' . $args['showAll'] . '">Show All</a></div>';	
				
		} else {
			
			echo 'no ' . $name . ' yet.';
			
		}
		
		echo '</div>';
		
	} // addDashHelper
	
	
} // beechbot_utilities
