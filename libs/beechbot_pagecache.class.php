<?php
	
/**
*
* @todo :
* admin to enable cahe
* admin to clear cache
*/
class beechbot_pagecache {
	
	
	/**
	*
	*/
	public static function getPage() {
		
		$cache = new beechbot_cache();
				
		if( isset($_GET['BEES_CAPTURE']) || !empty($_POST) || !$cache->isEnabled() || is_admin() ) return true;
		
		global $wp;
		$URL = home_url( add_query_arg([], $wp->request));	
			
		$cached = $cache->get($URL);
		
		if(!is_null($cached)) {
			echo $cached; exit;
		} else {
			self::capturePage($URL);
		}
		
	} // getPage()
	
	
	/**
	*
	*/
	public static function capturePage($URL) {
		
		$cache = new beechbot_cache();
		$URL = substr($URL, -1) != '/' ? $URL . '/' : $URL;
		$curlURL = $URL . '?' .  http_build_query(['BEES_CAPTURE' => 'YARP!']);
					
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $curlURL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$html = curl_exec($ch);
		curl_close($ch);
		
		$html = beechbot_pagecache::minifyHTML($html);
		$cache->set($URL, $html);
				
		return $html;
		
	} // capturePage()
	
	
	/**
	*
	*/
	public static function minifyHTML($html) {
		
		$search = [
			'/\>[^\S ]+/s',     // strip whitespaces after tags, except space
			'/[^\S ]+\</s',     // strip whitespaces before tags, except space
			'/(\s)+/s',         // shorten multiple whitespace sequences
			'/<!--(.|\s)*?-->/' // Remove HTML comments
		];
		
		$replace = [
			'>',
			'<',
			'\\1',
			''
		];
		
		$html = preg_replace($search, $replace, $html);
		
		return $html;
		
	} // minifyHTML()
	
	
	/**
	*
	*/
	public static function handleCacheOnSave() {
		
		global $post;
		
		// do not save if this is an auto save routine
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return $post->ID;
		
		$cache = new beechbot_cache();
		$cache->clearAll();

		
	} // handleCacheOnSave()
		
}	

add_action('get_header', function() {
		
	beechbot_pagecache::getPage();
	
});

add_action( 'save_post', function() {
	
	beechbot_pagecache::handleCacheOnSave();
	
});

