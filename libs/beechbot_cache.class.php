<?php
	
/**
* must have defined 'BEECHBOT_CACHE_ENABLED'
* optionally define BEECHBOT_CACHE_SEC
*/
class beechbot_cache {
	
	
	/**
	*
	*/
	public function __construct() {
		
		$this->getCacheType();
		$this->expire_seconds = defined('BEECHBOT_CACHE_SEC') ? BEECHBOT_CACHE_SEC : 300; // default to 5min cache (300s)
		$this->source = null;
		$this->content = null;
		
		return $this;
		
	} // __construct 
	
	
	/**
	*
	*/
	private function getCacheType() {
		
		switch(true) {
			
			case(!defined('BEECHBOT_CACHE_ENABLED') || !BEECHBOT_CACHE_ENABLED):
				$this->cache_type = 'narp';
				break;
			
			case(defined('BEECHBOT_CACHE_TYPE')):
				$this->cache_type = BEECHBOT_CACHE_TYPE;
				break;
			
			case(extension_loaded('apc') && ini_get('apc.enabled') && function_exists('apc_exists') && function_exists('apc_fetch') && function_exists('apc_add') && function_exists('apc_clear_cache')):
				$this->cache_type = 'APC';
				break;
				
			case(extension_loaded('xcache') && ini_get('xcache.size') != 0 && ini_get('xcache.var_size') != 0):
				$this->cache_type = 'xcache';
				break;
				
			default:
				$this->cache_type = 'narp';
				break;
			
		}

		if(!defined('BEECHBOT_CACHE_TYPE')) {
			define('BEECHBOT_CACHE_TYPE', $this->cache_type);			
		}
		
	} // getCacheType()
	
	
	/**
	*
	*/
	public function isEnabled() {
		
		return $this->cache_type == 'narp' ? false : true;
		
	} // isEnabled()
	
	
	/**
	* @return :  null || content
	*/
	public function get($key) {
		
		switch($this->cache_type) {
			
			case('APC'):	
				$this->content = apc_exists($key) ? apc_fetch($key) : null;
				$this->source = "APC";
				break;
			
			case('xcache'):	
				$this->content = xcache_isset($key) ? xcache_get($key) : null;
				$this->source = "xcache";
				break;
			
		}
		
		
		return $this->content;
				
	} // get()
	
	
	/**
	*
	*/
	public function set($key, $content, $cacheFor=null) {
		
		$cacheFor = !is_null($cacheFor) && is_numeric($cacheFor) ? $cacheFor : $this->expire_seconds;
		
		switch($this->cache_type) {
			
			case('APC'):	
				apc_add($key, $content, $cacheFor);
				break;
			
			case('xcache'):	
				xcache_set($key, $content);
				break;
			
		}
		
	} // set()
	
	
	/**
	*
	*/
	public function clear($key) {
		
		switch($this->cache_type) {
			
			case('APC'):	
				apc_delete($key);
				break;
			
			case('xcache'):	
				xcache_unset($key);
				break;
			
		}
		
	} // __unset()
	
	
	/**
	*
	*/
	public function clearAll() {
		
		switch($this->cache_type) {
			
			case('APC'):	
				apc_clear_cache();
				break;
			
			case('xcache'):	
				xcache_clear_cache();
				break;
			
		}
		
	} // clear
	
}	


add_action('get_header', function() {
		
	if(isset($_GET['beechbot_cache']) && $_GET['beechbot_cache'] == 'clearall') {
		$cache = new beechbot_cache();
		$cache->clearAll();
	}
	
});