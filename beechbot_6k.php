<?php
	
/**
Plugin Name: beechbot 6000
Description: Settings and stuff!
Author: beechbot
Version: 1.0
Author URI: http://beechbot.com
*/
class bb6k {
	
	
	/**
	*
	*/
	public function __construct() {
		
		$this->cleanResponseHeaders();
		$this->addTheLibs();
		$this->optimizeHead();
		$this->disableRestAPI();
		$this->addPrePop();
		$this->obfuscateWpDirs();
		
	} // __construct()
	
	
	/**
	* add the libs
	*/
	private function addTheLibs() {
		
		$folder = __DIR__ . '/libs/';
		$ignore = [".", "..", ".svn", ".git", ".DS_Store"];	
		
		if(is_dir($folder) && $handle = opendir($folder)) {
		
		    while (false !== ($file = readdir($handle))) {
		    		    	
		        if (is_file($folder . $file) && !in_array($file, $ignore))
		        	include_once($folder . $file);  
		        		        	
		    }
		    closedir($handle);
		    
		}
		
	} // addTheLibs()
	
	
	/**
	*
	*/
	public static function optimizeHead($keep=[]) {
		
		// remove emoji junk from head :
		if(!isset($keep['emoji'])) {
			remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
			remove_action( 'wp_head', 'wp_resource_hints', 2 );
			remove_action( 'wp_print_styles', 'print_emoji_styles' );
			remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
			remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
		}
		
		// remove generator :
		if(!isset($keep['generator'])) {
			remove_action('wp_head', 'wp_generator');	
		}
		
		// remove rsd :
		if(!isset($keep['rsd'])) {
			remove_action('wp_head', 'rsd_link');
			remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
		}
		
		// remove manifest
		if(!isset($keep['manifest'])) {
			remove_action('wp_head', 'wlwmanifest_link');
		}
		
		// remove oembed
		if(!isset($keep['oembed'])) {
			remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
		}
	
		
	} // optimizeHead()
	
	
	/**
	*
	*/
	public function disableRestAPI() {
		
		add_filter('json_enabled', '__return_false');
		add_filter('json_jsonp_enabled', '__return_false');
		add_filter('rest_enabled', '__return_false');
		add_filter('rest_jsonp_enabled', '__return_false'); 
	
	} // disableRestAPI()
	
	
	/**
	*
	*/
	public function cleanResponseHeaders() {
		
		add_filter('wp_headers', function($headers) {
		
			unset($headers['X-Pingback']);
			
			return $headers;
		
		});
		
		remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
	
	} // cleanResponseHeaders()
	
	
	/**
	*
	*/
	public function addPrePop() {
		
		/**
		* this checks the fields and whatnot
		*/	
		function pre_pop() {
			
			if(isset($_GET['prePop'])) {
				
				$prePop = [
					'cat' => isset($_GET['prePop-cat']) ? $_GET['prePop-cat'] : false,
					'parent' => isset($_GET['prePop-parent']) ? $_GET['prePop-parent'] : false,
					'template' => isset($_GET['prePop-template']) ? $_GET['prePop-template'] : false
				];
				
				$prePop = json_encode($prePop);
			
			?>
				
				<script type="text/javascript">
					
					;(function beechbot_pre_pop($) {
						
						var prePop = <?php echo $prePop; ?>;
						
						$(document).ready(function() {
							
							console.log('prePop : ', prePop);
							
							if(!!prePop.cat)
								$("#in-category-" + prePop.cat).attr('checked', 'checked');
							
							if(!!prePop.parent)
								$('[name="parent_id"]').find('[value="' + prePop.parent + '"]').attr('selected', 'selected');
								
							if(!!prePop.template)
								$('[name="page_template"]').find('[value="' + prePop.template + '"]').attr('selected', 'selected');		
							
							console.log('derp : ', $('[name="page_template"]').find('[value="' + prePop.template + '"]'));
						
						});
						
					
					}(jQuery));
					
				</script>
			
			<?php	
				
			}
			
		} //
		// add_meta_boxes
		add_action('admin_footer', 'pre_pop');
		
	} // addPrePop()
	
	
	/**
	*
	*/
	public function obfuscateWpDirs() {
		
		function obfuscateWp($content) {
			$styleSheetDir = get_stylesheet_directory();
			$bits = explode( '/themes/', $styleSheetDir );
			$theme_name = next( $bits );
			global $wp_rewrite;
			$roots_new_non_wp_rules = [
				'style.min.css(.*)' => 'wp-content/themes/' . $theme_name . '/style.min.css$1',
				'style.css(.*)' => 'wp-content/themes/' . $theme_name . '/style.css$1',
				'css/(.*)' => 'wp-content/themes/' . $theme_name . '/css/$1',
				'js/main.min.js(.*)'  => 'wp-content/themes/' . $theme_name . '/js/main.min.js$1',
				'js/(.*)'  => 'wp-content/themes/' . $theme_name . '/js/$1',
				'img/(.*)' => 'wp-content/themes/' . $theme_name . '/img/$1',
				'fonts/(.*)' => 'wp-content/themes/' . $theme_name . '/fonts/$1',
				'media/(.*)'  => 'wp-content/uploads/$1',
			];
			$wp_rewrite->non_wp_rules += $roots_new_non_wp_rules;
		}
		
		add_action( 'generate_rewrite_rules', 'obfuscateWp' );
	}
	
	
} // bb6k

new bb6k();




?>